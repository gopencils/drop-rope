﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Hole : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Target"))
        {
            other.transform.DOMove(new Vector3(transform.position.x, transform.position.y, 1), 0.3f);
            Destroy(other.gameObject, 3);
        }
    }
}
