﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LosePlane : MonoBehaviour
{
    public GameController gameController;
    private void OnCollisionEnter(Collision other) {
        if(other.gameObject.CompareTag("Target"))
        {
            // gameController.Lose();
            Destroy(other.gameObject);
        }
    }
}
