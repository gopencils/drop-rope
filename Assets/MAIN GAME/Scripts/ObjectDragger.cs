﻿using UnityEngine;
using System.Collections;
 
public class ObjectDragger : MonoBehaviour 
{
 
	private Vector3 screenPoint;
	private Vector3 offset;
	 
	void OnMouseDown()
	{
		screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
	}
	
	void OnMouseDrag()
	{
		Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
		var dir = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
		transform.position = new Vector3(Mathf.Clamp(dir.x, -2.3f, 2.3f), Mathf.Clamp(dir.y, -4, 4), transform.position.z);
	}
 }