﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using UnityEngine.EventSystems;
using System.Linq;
using Obi;

public class GameController : MonoBehaviour
{
    [Header("Variable")]
    public static GameController instance;
    public int maxLevel;
    public bool isStartGame = false;
    public bool isControl = false;
    int maxPlusEffect = 0;
    bool isVibrate = false;
    Rigidbody rigid;
    public float speed;
    Vector3 dir, firstPos, lastPos;
    bool isDrag = false;
    public float shakeSpeed;
    public Vector3 m_EulerAngleVelocity;
    public bool isShake = false;
    int ropeID = 0;
    private Vector3 screenPoint;
	private Vector3 offset;
    public LayerMask clickMask;
    public LayerMask dragMask;
    public int numOfRope;
    public ObiSolver solver;
    static int currentTask = 0;
    ObiRopeCursor cursor;
	public ObiRope rope;
	public float minLength = 0.1f;

    [Header("UI")]
    public GameObject winPanel;
    public GameObject losePanel;
    public Text currentLevelText;
    public int currentLevel;
    public Canvas canvas;
    public GameObject startGameMenu;
    public InputField levelInput;
    public InputField relaxInput;
    public GameObject nextButton;
    public Image title;
    public GameObject buttonStartGame;
    public Text timer;
    public Image star1, star2, star3;
    public List<Image> listRopeFill = new List<Image>();
    public List<Image> listTask = new List<Image>();
    static int currentBG = 0;
    public GameObject dropButton;

    public Text status;

    [Header("Objects")]
    public GameObject plusVarPrefab;
    public GameObject conffeti;
    GameObject conffetiSpawn;
    public GameObject ropePrefab;
    public GameObject markPrefab;
    public List<Transform> listRopes = new List<Transform>();
    public List<GameObject> listLevel = new List<GameObject>();
    public List<Color> listRopeColor = new List<Color>();
    public List<Color> listBGColor = new List<Color>();
    public GameObject targetObject;
    Transform startPoint;
    Transform endPoint;
    public GameObject BG;
    public GameObject blast;
    public GameObject ball;
    public GameObject round;

    private void OnEnable()
    {
        // PlayerPrefs.DeleteAll();
        Application.targetFrameRate = 60;
        instance = this;
        rigid = GetComponent<Rigidbody>();
        StartCoroutine(delayStart());
    }

    IEnumerator delayStart()
    {
        currentLevel = PlayerPrefs.GetInt("currentLevel");
        currentLevelText.text = "LEVEL " + currentLevel.ToString();
        var colorID = Random.Range(0,5);
        ropePrefab.transform.GetChild(0).GetComponent<Renderer>().material.color = listRopeColor[colorID];
        ropePrefab.transform.GetChild(0).GetComponent<ObiParticleRenderer>().particleColor = listRopeColor[colorID];
        BG.GetComponent<Renderer>().material.color = listBGColor[currentBG];
        currentBG++;
        if(currentBG > listBGColor.Count - 1)
        {
            currentBG = 0;
        }
        startGameMenu.SetActive(true);
        isControl = true;
        yield return new WaitForSeconds(0.01f);
        for(int i = 0; i < 100; i++)
        {
            var item = Instantiate(ball);
        }
        yield return new WaitForSeconds(0.5f);
        round.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        buttonStartGame.SetActive(true);
        title.DOColor(new Color32(255,255,255,0), 3);
        Destroy(title, 3);
        // TenjinManager.Instance.LogGame_startEvent();
        //AnalyticsManager.instance.CallEvent(AnalyticsManager.EventType.StartEvent);
    }

    private void Update()
    {
        if (isStartGame && isControl)
        {

        }
        else if(!isStartGame && isControl)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ButtonStartGame();
            }
        }
    }

    public void PlusEffectMethod()
    {
        if (maxPlusEffect < 10)
        {
            Vector3 posSpawn = timer.transform.position;
            StartCoroutine(PlusEffect(posSpawn));
        }
    }

    IEnumerator PlusEffect(Vector3 pos)
    {
        maxPlusEffect++;
        if (!UnityEngine.iOS.Device.generation.ToString().Contains("5") && !isVibrate)
        {
            isVibrate = true;
            StartCoroutine(delayVibrate());
            MMVibrationManager.Haptic(HapticTypes.LightImpact);
        }
        var plusVar = Instantiate(plusVarPrefab);
        plusVar.transform.SetParent(canvas.transform);
        plusVar.transform.localScale = new Vector3(1, 1, 1);
        //plusVar.transform.position = worldToUISpace(canvas, pos);
        plusVar.transform.position = new Vector3(pos.x + Random.Range(-50,50), pos.y + Random.Range(-100, -75), pos.z);
        plusVar.GetComponent<Text>().DOColor(new Color32(255, 255, 255, 0), 1f);
        plusVar.SetActive(true);
        plusVar.transform.DOMoveY(plusVar.transform.position.y + Random.Range(50, 90), 0.5f);
        plusVar.transform.DOMoveX(timer.transform.position.x, 0.5f);
        Destroy(plusVar, 0.5f);
        yield return new WaitForSeconds(0.01f);
        maxPlusEffect--;
    }

    IEnumerator delayVibrate()
    {
        yield return new WaitForSeconds(0.2f);
        isVibrate = false;
    }

    public Vector3 worldToUISpace(Canvas parentCanvas, Vector3 worldPos)
    {
        Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
        Vector2 movePos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
        return parentCanvas.transform.TransformPoint(movePos);
    }

    public void ButtonStartGame()
    {
        startGameMenu.SetActive(false);
        isStartGame = true;
        isControl = true;
    }

    IEnumerator Win()
    {
        float time = 4f;
        var timePanel = timer.transform.parent;
        timePanel.gameObject.SetActive(true);
        timePanel.transform.GetChild(0).GetComponent<Image>().DOFillAmount(1, 3);
        while (time >= 1.02f)
        {
            time -= 0.02f;
            timer.text = ((int)time).ToString();
            yield return null;
        }
        timePanel.gameObject.SetActive(false);
        if (isStartGame)
        {
            TenjinManager.Instance.LogGame_endEvent();
            //AnalyticsManager.instance.CallEvent(AnalyticsManager.EventType.EndEvent);
            Debug.Log("Win");
            isStartGame = false;
            losePanel.SetActive(false);
            status.text = "DONE";
            listTask[currentTask].DOFillAmount(1, 0.5f);
            currentTask++;
            if (currentTask == 3)
            {
                conffetiSpawn = Instantiate(conffeti);
                currentLevel++;
                if (currentLevel > maxLevel)
                {
                    currentLevel = 0;
                }
                PlayerPrefs.SetInt("currentLevel", currentLevel);
                yield return new WaitForSeconds(2);
                winPanel.SetActive(true);
                dropButton.SetActive(false);
                currentTask = 0;
            }
            else
            {
                blast.SetActive(false);
                blast.SetActive(true);
                yield return new WaitForSeconds(1);
                Camera.main.transform.DOMoveX(-20, 1);
                yield return new WaitForSeconds(1);
                LoadScene();
            }
        }
    }

    public void Lose()
    {
        if (isStartGame)
        {
            TenjinManager.Instance.LogGame_endEvent();
            //AnalyticsManager.instance.CallEvent(AnalyticsManager.EventType.EndEvent);
            Debug.Log("Lose");
            isStartGame = false;
            StartCoroutine(delayLose());
        }
    }

    IEnumerator delayLose()
    {
        yield return new WaitForSeconds(1);
        status.text = "OPPS!!";
        dropButton.SetActive(false);
        losePanel.SetActive(true);
    }

    IEnumerator timeScanner()
    {
        var timePanel = timer.transform.parent;
        float angle = -1200;
        float a = Time.deltaTime * -360;
        while (angle < 0)
        {
            timePanel.transform.GetChild(0).RotateAround(timePanel.transform.position, Vector3.forward, a);
            angle += a;
            yield return null;
        }
        timePanel.transform.GetChild(0).RotateAround(timePanel.transform.position, Vector3.forward, angle);
    }

    public void LoadScene()
    {
        winPanel.SetActive(false);
        losePanel.SetActive(false);
        var temp = conffetiSpawn;
        Destroy(temp);
        SceneManager.LoadScene(0);
    }

    public void OnChangeMap()
    {
        if (levelInput != null)
        {
            int level = int.Parse(levelInput.text.ToString());
            Debug.Log(level);
            if (level < maxLevel)
            {
                PlayerPrefs.SetInt("currentLevel", level);
                SceneManager.LoadScene(0);
            }
        }
    }

    public void OnChangeRelax()
    {
        if (relaxInput != null)
        {
            float level = float.Parse(relaxInput.text.ToString());
            level *= 0.1f;
            solver.distanceConstraintParameters.SORFactor = level;
            solver.UpdateParameters();
            PlayerPrefs.SetFloat("currentRelax", level);
        }
    }

    public void ButtonNextLevel()
    {
        title.DOKill();
        isStartGame = true;
        currentLevel++;
        if (currentLevel > maxLevel)
        {
            currentLevel = 0;
        }
        PlayerPrefs.SetInt("currentLevel", currentLevel);
        SceneManager.LoadScene(0);
    }

    public void TestDrop()
    {
        dropButton.transform.DOKill();
        StartCoroutine(delayDrop());
    }

    IEnumerator delayDrop()
    {
        dropButton.SetActive(false);
        isControl = false;
        foreach(var item in listRopes)
        {
            item.transform.DOMoveY(2f, 0.5f);
            item.transform.GetChild(0).GetComponent<QuickOutline>().enabled = false;
        }
        solver.distanceConstraintParameters.SORFactor = 0.1f;
        solver.UpdateParameters();
        yield return new WaitForSeconds(0.5f);
        // float level = PlayerPrefs.GetFloat("currentRelax");
        // if(level <= 0)
        // {
        //     level = 3;
        // }
        targetObject.GetComponent<Rigidbody>().isKinematic = false;
        StartCoroutine(Win());
    }
}
