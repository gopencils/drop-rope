using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TenjinManager : MonoBehaviour
{
    public string API_KEY;
    public static TenjinManager Instance { get; private set; }
    private void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        BaseTenjin baseTenjin = Tenjin.getInstance(API_KEY);
        baseTenjin.Connect();
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            //do nothing
        }
        else
        {
            BaseTenjin baseTenjin = Tenjin.getInstance(API_KEY);
            baseTenjin.Connect();
        }
    }

    public void LogGame_startEvent()
    {
        //event with name
        BaseTenjin baseTenjin = Tenjin.getInstance(API_KEY);

        //event with name and integer value
        baseTenjin.SendEvent("gameStart", GameController.instance.currentLevel.ToString());
    }

    public void LogGame_endEvent()
    {
        //event with name
        BaseTenjin baseTenjin = Tenjin.getInstance(API_KEY);

        //event with name and integer value
        baseTenjin.SendEvent("gameEnd", GameController.instance.currentLevel.ToString());
    }
}
