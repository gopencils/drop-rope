﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

public class CursorController : MonoBehaviour {

	public ObiRopeCursor cursor;
	public ObiRope rope;
	public float minLength = 0.1f;
	public Transform a,b;
	float distance;
	bool isInitRope = false;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.W)){
			if (rope.RestLength > minLength)
				cursor.ChangeLength(rope.RestLength - 1f * Time.deltaTime);
		}

		if (Input.GetKey(KeyCode.S)){
			cursor.ChangeLength(rope.RestLength + 1f * Time.deltaTime);
		}

		if (Input.GetKey(KeyCode.A)){
			rope.transform.Translate(Vector3.left * Time.deltaTime,Space.World);
		}

		if (Input.GetKey(KeyCode.D)){
			rope.transform.Translate(Vector3.right * Time.deltaTime,Space.World);
		}

        if (!isInitRope)
        {
            distance = Vector3.Distance(a.position, b.position);
            if (rope.RestLength < distance * 2)
            {
                cursor.ChangeLength(rope.RestLength + 1f * Time.deltaTime);
            }
            else
            {
                isInitRope = true;
            }
        }
	}
}
